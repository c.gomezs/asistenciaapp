//
//  ViewController.swift
//  AsistenciaApp
//
//  Created by CAMILA GOMEZ SCHRADER on 8/12/19.
//  Copyright © 2019 CAMILA GOMEZ SCHRADER. All rights reserved.
//

import UIKit
import CoreBluetooth
import Foundation


class ViewController: UIViewController {
    
    @IBOutlet weak var button1: UIButton!
    
    @IBOutlet weak var button2: UIButton!
    
    @IBOutlet weak var button3: UIButton!
    
    var peripheralManager: CBPeripheralManager!
    
    @IBOutlet weak var button4: UIButton!
    
    @IBOutlet weak var button5: UIButton!
    
    var mensaje: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button1.layer.cornerRadius=10
        
        button1.layer.borderWidth=2
        
        button1.layer.borderColor = UIColor.black.cgColor
        
        button2.layer.cornerRadius=10
        
        button2.layer.borderWidth=2
        
        button2.layer.borderColor = UIColor.black.cgColor
        
        button3.layer.cornerRadius=10
        
        button3.layer.borderWidth=2
        
        button3.layer.borderColor = UIColor.black.cgColor
        
        button4.layer.cornerRadius=10
        
        button4.layer.borderWidth=2
        
        button4.layer.borderColor = UIColor.black.cgColor
        
        button5.layer.cornerRadius=10
        
        button5.layer.borderWidth=2
        
        button5.layer.borderColor = UIColor.black.cgColor
        
        self.view.addSubview(button1)
        self.view.addSubview(button2)
        self.view.addSubview(button3)
        self.view.addSubview(button4)
        self.view.addSubview(button5)
        
        mensaje=""
    }
    
    
    @IBAction func click(_ sender: Any) {
        
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        print("Started advertising")
        
    }
    
  
    @IBAction func clickApagar(_ sender: Any) {
        peripheralManager?.stopAdvertising();
    }
    
    @IBAction func clickVerificar(_ sender: Any) {
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "d-M-yyyy"
        let fecha = format.string(from: date)
        let arreglo = fecha.components(separatedBy: "-")
        let dia = arreglo[0]
        let mes = String(Int(arreglo[1])! - 1)
        let anio = arreglo[2]
        
        let alertController2=UIAlertController (title: "Error", message: "No estas registrada", preferredStyle: .alert)
        alertController2.addAction(UIAlertAction (title: "De One", style: .default))
        
        let alertController=UIAlertController (title: "Registro Exitoso", message: "Ya te encuentras registrado", preferredStyle: .alert)
        alertController.addAction(UIAlertAction (title: "De One", style: .default))
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + dia + "-" + mes + "-" + anio + "/students/201531412")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else { self.present(alertController2, animated: false, completion: nil); return}
            
            self.present(alertController, animated: false, completion: nil)
            
        }
        task.resume()

        
    }
    
    @IBAction func clickAusentes(_ sender: Any) {
        
        //variables de la fecha de hoy
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "d-M-yyyy"
        let fecha = format.string(from: date)
        let arreglo = fecha.components(separatedBy: "-")
        let dia = arreglo[0]
        let mes = String(Int(arreglo[1])! - 1)
        let anio = arreglo[2]
        
        //Arreglo para guardar los estudiantes registrados
        var estudiantes: [String] = []
        
        //Arreglo de estudiantes presentes
        var presentes: [String] = []
        
        
        //Primera peticion - Lista 
        //URL de la base de datos de la lista de estudiantes
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList")!
        
        //Peticion HTTP
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in guard let data = data else {return}
        //print(String(data: data, encoding: .utf8) ?? "default")
            
            let json=try? JSONSerialization.jsonObject(with: data, options: [])
            //print(json)
            if let dictionary = json as? [String: Any]{
                let doc = dictionary["documents"]
                
                if let documents = doc as? [Any]
                {
                    //print(documents.first)
                    for object in documents
                    {
                        if let obj = object as? [String: Any]
                        {
                            let f = obj["fields"] as! [String: Any]
                            
                            let code = f["code"] as! [String: Any]
                            let name = f["name"] as! [String: Any]
                            
                            let c = code["stringValue"] as! String
                            let n = name["stringValue"] as! String
                            
                            let msg = c + "," + n
                            
                            estudiantes.append(msg)
                        }
                    }
                }
                print(estudiantes)
            }
        }
        task.resume()
        
        //Segunda petici[on lista de presentes
        //URL de la base de datos de la lista de presentes del dia
        let url2 = URL(string:"https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students")!
        //print("hola")
        //Peticion HTTP
        let task2 = URLSession.shared.dataTask(with: url2) {(data, response, error) in guard let data = data else {return}
            //print(String(data: data, encoding: .utf8) ?? "default")
            
            let json=try? JSONSerialization.jsonObject(with: data, options: [])
            //print(json)
            if let dictionary = json as? [String: Any]{
                let doc = dictionary["documents"]
                
                if let documents = doc as? [Any]
                {
                    //print(documents.first)
                    for object in documents
                    {
                        if let obj = object as? [String: Any]
                        {
                            let f = obj["fields"] as! [String: Any]
                            
                            let code=f["code"] as! [String: Any]
                            
                            presentes.append(code["stringValue"] as! String)
                        }
                    }
                }
                //print(presentes)
                
            }
        }
        task2.resume()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            var ausentes: [String] = []

            print(estudiantes.isEmpty)
            for est in estudiantes {
                let cod = est.components(separatedBy: ",")[1]
                print(cod)
                var esta = false
                for pres in presentes {
                    print(pres)
                    if (cod == pres) {
                        esta = true
                    }
                }
                
                if (esta == false ) {
                    ausentes.append(est)
                }
            }
            
            for aus in ausentes {
                self.mensaje += aus + "\n"
            }
            
            print("mensaje: " + self.mensaje)
            let alertController=UIAlertController (title: "Ausentes", message: self.mensaje, preferredStyle: .alert)
            alertController.addAction(UIAlertAction (title: "De One", style: .default))
            self.present(alertController, animated: true, completion: nil)
        })
    }
    
    
    @IBAction func clickCSV(_ sender: Any) {
        let fileName = "Ausentes.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)!
        
        do {
            try mensaje.write(to: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        
        let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
        present(vc, animated: true, completion: nil)
        
    }
    
}

extension ViewController: CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print(peripheral.state.rawValue)
        
        if(peripheral.state.rawValue == CBManagerState.poweredOn.rawValue)
        {
            let advertisementData = [CBAdvertisementDataLocalNameKey: "CGS"]

            peripheralManager?.startAdvertising(advertisementData)
        }
        
    }
    
    
    
}
